Example of a containerized shared memory application using ipc: host



```
# Build
docker-compose build

# Run
docker-compose up

# Destroy
docker-compose down
```



>**Reference:**
https://www.youtube.com/watch?v=SMeDw2GDMsE
